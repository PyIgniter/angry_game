
"""

"""
import pygame

pygame.init()

display_width = 400
display_height = 500

windows = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('Hello, Pygame')

gameDisplay = pygame.Surface((display_width, display_height))
done = True

while done:
    windows.blit(gameDisplay, (0,0))
    pygame.display.flip()
